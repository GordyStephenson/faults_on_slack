# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 11:43:01 2019

FaultPlotter.py
A compiled Python routine to automate production of plots for Fault Events.
Produces plots for ONE fault event at a time.
Takes as arguments UNIT, INSTALLCODE, FAULTTYPE, TIME, and OUTPUT_FILENAME 

This will interface with the DataMonitor program Mike He (software) and Spencer? Maas?
are working on.

Given a Unit, InstallCode, Time, and FaultType, download that flywheel's data from the cloud
and produce a plot with relevant variables.

Inputs / Opts:
    -u <Unit> 
    -i <InstallCode>
    -f <FaultType> 
    -t <Time>
 Outputs:
     -o <outputfile>')

@author: GordonStephenson
"""
#!/usr/bin/python

### Import Statements.
import sys, getopt
from pathlib import Path
import datetime as dt
import subprocess
import pandas as pd
import matplotlib.pyplot as plt
from math import ceil
import os.path
# Maybe won't need the email stuff???
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

### Define colors?
#colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)

mypath = os.path.abspath(os.path.dirname(__file__))

with open( os.path.join(mypath, 'faultVariables.config') ) as f:
    header = f.readline()
    faultVars = {}
    for n, line in enumerate(f):
        tokens = line.strip().split(',')
        faultVars[tokens[0].strip()] = [tok.strip() for tok in tokens[1::]]

#### Read configuration file that controls the plot colors.
fColors = pd.read_csv( os.path.join(mypath, 'faultColors.config') )
fColors.columns = fColors.columns.str.strip()
fcdict = dict( zip(fColors.Variable.str.strip(), fColors.Color.str.strip()) )

# Fault Descriptions

def plotFault(df, faultType = 'FaultType', unitName = 'TestUnit', faultDate=None):
    try:
        varlist = faultVars[ faultType ]
    except KeyError:
        print( faultType + ' not found. Check faultVariables.config.')
        varlist = ['MechFrequency', 'MBoardTemperature', 'Pressure', 'PowerDC']
    nVars = len(varlist)
    nRows = ceil( nVars/2.0)
    ySize = max(3, 1+3*nRows)
    fig, ax0 = plt.subplots(nrows=nRows, ncols=1, sharex=True, sharey=False, 
                            squeeze=True, figsize=(9,ySize))
    # Initialize this variable to control y-axis color.
    leftVar = 'Other'
    for n, var in enumerate( varlist ):
        rowNum = n//2
        # Alternate plotting on left y-axis and right y-axis.
        if n%2==0:
            axLeft = ax0[rowNum]
            axLeft.tick_params(axis='y', which='left', labelleft=True, labelright=False)            
            # Plot using color specified in faultColors.config
            #df[var].plot(ax=axLeft, c=fcdict[var], marker='.',markeredgecolor='k', markeredgewidth=0.1, linewidth=0.5)
            df[var].plot(ax=axLeft, c=fcdict[var], marker='.',linewidth=0.3)
            if n>1:
                # Offset left and right axes in y-direction.
                ylimL = axLeft.get_ylim()
                yCenter, ySpan = 0.5 * ( ylimL[0]+ylimL[1] ), 0.5* (ylimL[1] - ylimL[0] )
                axLeft.set_ylim( yCenter - ySpan, yCenter + 3 * ySpan )
            axLeft.set_ylabel(var)
            axLeft.spines['left'].set_linewidth(4.0)
            axLeft.spines['left'].set_color( fcdict[var])
            leftVar = var;
            if not pd.isnull( faultDate):
                plt.axvline(x=faultDate, color='k', linestyle='--')
        elif n%2==1:
            axRight = ax0[rowNum].twinx()
            axRight.tick_params(axis='y', which='right', labelleft=False, labelright=True)        
            # Plot using color specified in faultColors.config
            #df[var].plot(ax=axRight, c=fcdict[var], marker='.',markeredgecolor='k',markeredgewidth=0.1,linewidth=0.5)
            df[var].plot(ax=axRight, c=fcdict[var], marker='.',linewidth=0.3)
            if n>1:
                # Offset in y-direction.
                ylimR = axRight.get_ylim()
                yCenter, ySpan = 0.5 * ( ylimR[0]+ylimR[1] ), 0.5* (ylimR[1] - ylimR[0] )
                axRight.set_ylim( yCenter - 3* ySpan, yCenter + ySpan )
            axRight.set_ylabel(var)
            axRight.spines['left'].set_linewidth(4.0)
            axRight.spines['left'].set_color( fcdict[leftVar] )
            axRight.spines['right'].set_color(fcdict[var])
            axRight.spines['right'].set_linewidth(4.0)
            if not pd.isnull( faultDate):
                plt.axvline(x=faultDate, color='k', linestyle='--')
    titleStr = faultType + ' on ' + unitName + ' at ' + str(faultDate)
    fig.suptitle(titleStr, fontsize=16)
    return fig

# Default Plots:


#########################################
#   DATA GETTERS
# Include this function, although it won't be called from the DataMonitor.
# Download the event log. starttime, endtime are Datetime objects.
def getEventLog( starttime = None, endtime=None, unit="M32-P-F17", install=4, filename="eventsTemp.csv"):
    if endtime is None:
        endtime = dt.datetime.now() + dt.timedelta(days=9./24)
    if starttime is None:
        # Default to last twelve hours.
        starttime = endtime - dt.timedelta( 0, 60*60*12)
    elif isinstance( starttime, int ) or isinstance( starttime, float):
        starttime = endtime - dt.timedelta( days= abs(starttime) )
    ### Convert starttime, endtime to strings to construct download command.
    #### If there's a T, use: str.dt.datetime.now().replace(' ','T')[:-3]+'Z'
    startStr = str( starttime)[:-3] + 'Z'
    endStr   = str( endtime)[:-3] + 'Z'
#    cloudapp_path = Path("C:/Users/Gordon Stephenson/Dropbox (Amber Kinetics)/Engineering/Validation/Test Data/DataPlotting/AmberCloud/AmberCloud" )
    cloudapp_path = Path.home() / "Dropbox (Amber Kinetics)/FlywheelUnitSoftware/AmberCloud/AmberCloud"
    command_str = " downloadflywheelevents" + " -s " + '"' + startStr + '"' + " -e " + '"' + endStr + '"' + " -n " + '"'+unit+'"' + " -i " + str(install) + " -f " + filename
    print( str(cloudapp_path) + command_str )
    subprocess.run( str(cloudapp_path) + command_str )

# Download the data. starttime, endtime are Datetime objects.
    #### TODO: Consider adding a filepath for the download.
def getRecentData( starttime = None, endtime=None, unit="M32-P-F17", install=4, filename="dataTemp.csv"):
    if endtime is None:
        endtime = dt.datetime.now()
    if starttime is None:
        # Default to last twelve hours.
        starttime = endtime - dt.timedelta( 0, 60*60*12)
    ### Convert starttime, endtime to strings to construct download command.
    #### If there's a T, use: str.dt.datetime.now().replace(' ','T')[:-3]+'Z'
    startStr = str( starttime)[:-3] + 'Z'
    endStr   = str( endtime)[:-3] + 'Z'
    cloudapp_path = Path.home() / "Dropbox (Amber Kinetics)/FlywheelUnitSoftware/AmberCloud/AmberCloud"
    command_str = " downloadflywheelreadings" + " -s " + '"' + startStr.replace(' ','T') + '"' + " -e " + '"' + endStr.replace(' ','T') + '"' + " -n " + '"'+unit+'"' + " -i " + str(install) + " -f " + filename
    print( str(cloudapp_path) + command_str )
    subprocess.run( str(cloudapp_path) + command_str )

def downloadFaultData( faultTime, unit, install, filename="dataTemp.csv", left_window=2, right_window=2):
    startWindow = faultTime - dt.timedelta( 0, left_window*60)
    endWindow = faultTime + dt.timedelta( 0, right_window*60)
    getRecentData( startWindow, endWindow, unit, install, filename)

########################################################################
### Get window of data centered on FaultTime.
# window_size in minutes.
# Centertime is a Datetime, not a Timestamp. 
# Convert Timestamp using Timestamp.to_pydatetime() before calling.
def windowData( data, centerTime, left_window=2, right_window=2):
    startWindow = centerTime - dt.timedelta( 0, left_window*60)
    endWindow = centerTime + dt.timedelta( 0, right_window*60)
    return data[startWindow:endWindow]

########################################################################
#### Function to read in .csv file and do datetime conversion.
def load_and_convert( filename):
    #print( filename )
    df = pd.read_csv( filename, index_col=0)
    #print( df['Time'].head() )
    df.columns = df.columns.str.strip()
    # One incomplete eventTemp file is giving me a hard time --
    # the date is '2', instead of '2019-04-26...'
    # Drop all rows that don't have YYYY-MM-DD == at least ten characters for time.
    df = df[ [len(k)>9 for k in df.Time] ]
    try:
        df['Time'] = pd.to_datetime( df['Time'], infer_datetime_format=True)
        df = df.dropna( axis='index', subset=['Time'])        
        df.set_index('Time', inplace=True)
        df.sort_index( inplace=True )
    except Exception as e:
        print( 'Could not load and convert ', filename)
    #print( df.head)
    return df

########################################################################
#### Parse the event log for Faults.
def getFaultTimes( df ):
    faults = df['FaultType'].loc[ df['FaultType'].notnull() ]
    fault_list = [z for z in zip( faults.values, faults.index)]
#    fault_list = []
#    for n,s in enumerate(df['OperationState']):
#        if isinstance(s, str):
#            #print(s)
#            if 'Fault' in s:
#                fault_type = s
#                # Get time stamp of fault.
#                #ftime = df['Time'][n]
#                ftime = df.index[n]
#                fault_list.append([ fault_type, ftime ])
#            else:
#                pass
    return fault_list


########################################################################
#### Send a warning via email.
def emailWarning(toaddr, fromaddr, subject='FaultWarningTest', ImgFileList=[''], messageText='test' ):
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = fromaddr
    msg['To'] = toaddr
    text = MIMEText(''.join( messageText ) )
    print( text )
    msg.attach(text)
    for ImgFileName in ImgFileList:
        print( ImgFileName )
        img_data = open(ImgFileName, 'rb').read()
        image = MIMEImage(img_data, name=Path('.' / ImgFileName)  )#os.path.basename(ImgFileName))
        msg.attach(image)
    #s = smtplib.SMTP(Server, Port)
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    UserName = 'notifications@amberkinetics.com'
    UserPassword = 'yhgvihxjwgswwofr' #getpass.getpass('Password: ')    
    s.login(UserName, UserPassword)
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()

def testFP():
    argoggles = ['-u','M32-X-U21','-i','6','-t','2019-04-01T01:19:02Z','-f','ForceFault','-o','testFP.png']
    main( argoggles )
    argoggles = ['-u','M32-X-U21','-i','6','-t','2019-04-01T01:19:02Z','-f','Power24VFault','-o','testFP2.png']
    main( argoggles )
    argoggles = ['-u','M32-X-U21','-i','6','-t','2019-04-01T01:19:02Z','-f','PressureFault','-o','testFP3.png']
    main( argoggles )
    argoggles = ['-u','M32-X-U21','-i','6','-t','2019-04-01T01:19:02Z','-f','OverSpeedFault','-o','testFP4.png']
    main( argoggles )
    argoggles = ['-u','M32-X-U21','-i','6','-t','2019-04-01T01:19:02Z','-f','DCOverVoltage','-o','testFP4.png']
    main( argoggles )
    argoggles = ['-u','M32-X-U21','-i','6','-t','2019-04-01T01:19:02Z','-f','CPLDFault1','-o','testFP4.png']
    main( argoggles )
    
def main(argv):
    email=None
#    email = 'yes, please'
#    unitName="M32-P-F17"
#    installCode = '4'
#    faultType = 'PressureFault'
#    faultTime = "2019-03-04 10:00:00.000Z"
#    outfile = "practice.png"
    try:
        opts, args = getopt.getopt(argv,"hu:i:f:t:o:",["help","unit=","install=","fault=","time=","ofile="])
    except getopt.GetoptError:
        print('FaultPlotter.py -u <Unit> -i <InstallCode> -f <FaultType> -t <Time> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', "--help"):
            print('FaultPlotter.py -u <Unit> -i <InstallCode> -f <FaultType> -t <Time> -o <outputfile>')
            sys.exit()
        elif opt in ("-u", "--unit"):
            unitName = arg
        elif opt in ("-i", "--install"):
            installCode = arg
        elif opt in ("-f", "--fault"):
            faultType = arg
        elif opt in ("-t","--time"):
            faultTime = arg
        elif opt in ("-o", "--ofile"):
            outfile = arg
    try:
        print('Unit is ', unitName)
        print('InstallCode is ', installCode)
        print('FaultType is ', faultType)
        print('FaultTime is ', faultTime)
        print('Output file is ', outfile)
    except UnboundLocalError:
        print("Usage: \n:")
        print('FaultPlotter -u <Unit> -i <InstallCode> -f <FaultType> -t <Time> -o <outputfile>')
        sys.exit(2)

    #### Convert args_in to 
    unit = unitName   #unit = "M32-P-F17"
    install= int(installCode)   #install=4
    #event_file = "eventsTemp.csv"
    data_file = "dataTemp.csv"

    ## Download the Event log and data.
    #getEventLog(unit=unit, install=install, filename=event_file)
    #getRecentData(unit=unit, install=install, filename=data_file)
    #getRecentData( starttime = None, endtime=None, unit="M32-P-F17", install=4, filename="dataTemp.csv"):

    # Get data from +/-few minutes window around fault time.
    # Convert Timestamp to Datetime in calling windowData function.
    faultDatetime = pd.to_datetime( faultTime, infer_datetime_format=True)
    downloadFaultData( faultDatetime, unit, install, data_file)
    
    ## Read in the Event Log and convert times
    #events = load_and_convert( event_file )
    #print(events.head() )
    fData = load_and_convert( data_file )

    try:
        outfig = plotFault(fData, faultType, unitName, faultDatetime)
        outfig.savefig( outfile, bbox_inches="tight")
    except Exception as e:
        print( 'Error: No plots for ', faultType, ' on ', unit, ' at ', faultTime)
        print(e)
        
    if isinstance(email, str):
        ### Compose and Send message.
        #From = 'AKFaultMonitor@gmail.com'
        From = 'notifications@amberkinetics.com'
        To = 'gstephenson@amberkinetics.com'
        Subject = 'Fault Warnings for ' + unit
        faults = []
        try:
            emailWarning(toaddr=To, fromaddr=From, subject=Subject, ImgFileList = [outfile], messageText = str( faults ) )
        except Exception as e1:
            print( "Emailing didn't work")
            print(e1)
    
if __name__ == "__main__":
   main(sys.argv[1:])
