# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 09:36:27 2019

I want to access a list of flywheels of interest and their most up-to-date install codes.

@author: GordonStephenson
"""
import os
import subprocess
import pandas as pd
import numpy as np
import datetime as dt
import re

try:
    subprocess.run( os.path.join(os.path.expanduser('~'),'Desktop\\AzureDownload\\') + 'AzureInterfaceConsole.exe record')
except Exception as e:
    print( 'AzureDownload failed' )
    print(e)

def ReformatRecordDate( strTime ):
    """
    Some of the time strings I encountered have weird punctuation. 
    This code ignores the punctuation and converts a bad string to a datetime.
    """
    if not isinstance( strTime, str):
        return None
    try:
        YYYY, MM, DD, HH, mm, SS, subS = re.split('-| |:|T|\.', strTime)
    except ValueError:
        try:
            YYYY, MM, DD, HH, mm, SS = re.split('-| |:|T|\.', strTime)
        except ValueError:
            return None
    #print( YYYY, MM, DD, HH, mm, SS, subS )
    return dt.datetime( int(YYYY), int(MM), int(DD), int(HH), int(mm), int(SS) )

class UnitRecord:
    def __init__(self):
        self.getUnitRecord()
        self.rec=pd.DataFrame()
        self.flywheels = []
        self.latestInstall={}
        self.badInstalls = [('M32-X-U15',10)]
        self.buildLatestRecord()
        
    def getUnitRecord(online=True):
        #dataPath = os.path.join(os.path.expanduser('~'), 'Dropbox (Amber Kinetics)\\Engineering\\Software\\SCADA\\AzureDownload\\')
        #if online:
        #    subprocess.run(dataPath + '\AzureInterfaceConsole.exe record')
        downloadPath = os.path.join(os.path.expanduser('~'),'Dropbox (Amber Kinetics)\\FlywheelUnitSoftware\\AmberCloud\\')
        if online:
            subprocess.run( downloadPath + 'AmberCloud.exe record')

    def isFakeInstall(self, flywheel, install):
        return (flywheel, install) in self.badInstalls
        
    def buildLatestRecord(self):
        rec = pd.read_csv('RecordTemp.csv', index_col=False)
        rec.columns = rec.columns.str.strip()
        rec = rec.dropna( subset=['InstallTime'])
        rec['InstallTime'] = rec['InstallTime'].apply( ReformatRecordDate)
        rec['UninstallTime'] = rec['UninstallTime'].apply( ReformatRecordDate)
        self.rec = rec
        # Drop rows that correspond to known bad or weird installs.
        for (fw, ic) in self.badInstalls:
            badrow = rec[ (rec['UnitLabel']==fw) & (rec['InstallCode']==ic)]
            rec.drop( badrow.index, inplace=True)
        # Flywheels of interest are only M32 units, not on Ganymede, with number < 100.
        fwlist = [ fw for fw in set(rec['UnitLabel']) if fw.startswith('M32') and len(fw)<10]
        self.flywheels = fwlist
        latestInstallTime = {}
        latestInstallCode = {}
        for fw in fwlist:
            latestInstallTime[fw] = rec['InstallTime'][ rec['UnitLabel']==fw ].max()
            latestInstallCode[fw] = rec['InstallCode'][ (rec['UnitLabel']==fw) 
                                    & (rec['InstallTime']==latestInstallTime[fw])]
            self.latestInstall[fw] = [latestInstallCode[fw], latestInstallTime[fw]]
    
    def listFlywheels(self):
        return self.flywheels
    
    def listUnitAndCode(self):
        return [(fw, self.getLatestInstallCode(fw)) for fw in self.flywheels if self.getLatestInstallCode(fw) is not None]
    
    def getLatestInstallCode(self, flywheel ):
        try:
            return self.latestInstall[flywheel][0].values[0]
        except:
            return None

    def getLatestInstallAll(self):
        return [ (fw, self.getLatestInstallCode(fw), self.getLatestInstallTime(fw)) for fw in self.flywheels ]
    
    def getLatestInstallTime(self, flywheel ):
        if flywheel.lower()=='all':
            return self.getLatestInstallAll()
        else:
            return self.latestInstall[flywheel][1]

    def getInstallsTimes(self, flywheel):
        # Return a list of the installCodes, InstallTime, and UninstallTime.
        # For UninstallTime, use the earliest of [the listed UninstallTime, the
        # time of the next install, the current date.]
        installs = self.rec[['InstallCode','InstallTime','UninstallTime']][ self.rec['UnitLabel']==flywheel].sort_values(by='InstallTime')
        #if pd.isnull(installs):
        #    return None
        installs = installs.values
        nrows, ncols = installs.shape
        nowTime = str( dt.datetime.now() )
        # Adjust the UninstallTimes to be logical. (Uninstall before next Install).
        # If UninstsallTime is NaN, replace it by next InstallTime.
        for i1 in range(nrows-1):
            if pd.isnull( installs[i1,2]):
                installs[i1,2] = installs[i1+1,1]
            else:
                installs[i1,2] = min( installs[i1,2], installs[i1+1, 1], nowTime)
        # If the last entry is not a time, replace it by today's date.
        if pd.isnull( installs[-1,2] ):
            installs[-1,2]
        return installs.tolist()
    
            
def main():
    record = UnitRecord()
    unitCodes = record.listUnitAndCode()
    for u,c in unitCodes:
        print( u, ' : ', c )
    
if __name__=='__main__':
    main()
    