# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 15:19:47 2019

@author: Test
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 10:59:27 2019

Program to run on local machine 
        -- download faults, plot them, email them -> slack them.
To be set up on Maria's spare laptop, execute once/hour.

Combine with health monitoring program -- joint program that does 
health monitoring and fault reporting simultaneously.

@author: GordonStephenson
"""
import FaultPlotter as FP
import UnitRecord
import matplotlib.pyplot as plt
import os
import pandas as pd
import subprocess

def checkNHours( Nhours = 24, startTime=None):
    # How many hours back should we check?
    #Nhours = 3.1
    if pd.isnull( startTime ):
        startTime = -1.0*Nhours/24

    #Update the record so I know which flywheels to track.
    rec1 = UnitRecord.UnitRecord()
    activeFlywheels = rec1.listUnitAndCode()
    
    #Download event log from last hour for each active flywheel.
    eventFile = "eventTemp.csv"
    list_of_faults = []
    list_of_faultFigs = []
    faultDir = os.path.join(os.path.expanduser('~'), 'Dropbox (Amber Kinetics)\\Engineering\\Validation\\Test Data\\FaultPlotting\\Plotted_Faults\\')

    #activeFlywheels = [('M32-P-F11',2)]
    
    for unit, install in sorted(activeFlywheels):
        try:
            FP.getEventLog( starttime = startTime, unit=unit, install=install, filename= eventFile)
            events = FP.load_and_convert(eventFile)
            faultTimes = FP.getFaultTimes(events)
            #locationSite = [' '.join(['' if pd.isnull(x) else x for x in list1]) for list1 in rec1.rec[['Site','Location']].values]
            locationSite = rec1.rec[['Site','Location']][(rec1.rec['UnitLabel']==unit)&(rec1.rec['InstallCode']==install)]
            locationString = ' '.join( [x for x in locationSite.values.tolist()[0] if not pd.isnull(x) ] )
            # Download data for each fault found.
            for faultType, faultTime in faultTimes:
                windowAfter = 2
                if faultType in ['Power24VFault']:
                    # How many minutes after the fault do you want to download?
                    windowAfter = 5
                if faultType not in ['UserInitiated','NoFault']:
                    dataFile = "dataTemp.csv"
                    FP.downloadFaultData(faultTime=faultTime, unit=unit, install=install,
                                         right_window = windowAfter, filename=dataFile)
                    # Load the data file.
                    df = FP.load_and_convert( dataFile )
        
                    # Plot the fault.
                    fileStr = unit+'.'+str(install)+'_'+faultType+'_'+str(faultTime).replace(' ','T').replace(':','-')[:-3]+'.png'
                    list_of_faults.append(fileStr[:-7]+'_' + locationString)
                    print( fileStr)
                    outfile = faultDir + fileStr
                    try:
                        outfig = FP.plotFault(df, faultType=faultType, unitName=unit, faultDate=faultTime)
                    except TypeError as e:
                        print(unit+'.'+str(install)+' '+faultType+' '+str(faultTime)+'. Failed to plot.')
                        print(e)
                    label = outfig._suptitle.get_text()
                    outfig.suptitle(label[:-3] + ' UTC\n ' + locationString)
                    outfig.savefig( outfile )
                    list_of_faultFigs.append(fileStr)
                    plt.close( outfig )
        except Exception as e:
            print('\n Fault plotting failed for ' + unit + '.' + str(install) + '\n')
            print(e)
    # Send one email with all faults from last hour.
    # Or send one email per fault.
    localFaultDir = "C:\\Users\\Test\\Desktop\\Python Codes\\"
    #logFile = faultDir + 'faultPlotting_Log.csv'
    logFile = localFaultDir + 'faultPlotting_Log.csv'
    with open(logFile,'a') as f:
        for fault in list_of_faults:    
            unitID, faultType, datetime, location = fault.split('_')
            f.write(unitID + ' had a ' + faultType + ' at ' + datetime + ' in ' + location + '\n')
        
    #print( x for x in zip( list_of_faults, list_of_faultFigs))
    print( list_of_faults )
    return list_of_faults, list_of_faultFigs    
#curl -X POST --data-urlencode "payload={\"channel\": \"#fault_notifications\", \"text\": \"One more test of the default valuesy @Gordy Stephenson. Can we link to a <https://www.dropbox.com/preview/Engineering/Validation/Test%20Data/FaultPlotting/Plotted_Faults/M32-X-U27.6_MotorOverVoltage_2019-04-19T16-51-11.650000.png?role=work|file on Dropbox?>\"}" https://hooks.slack.com/services/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
#curl -X POST --data-urlencode "payload={\"channel\": \"#fault_notifications\", \"text\": \"test post.\", \"image_url\": \"https://www.dropbox.com/home/Engineering/Validation/Test%20Data/FaultPlotting/Plotted_Faults/M32-X-U27.6_MotorOverVoltage_2019-04-19T16-51-11.650000.png\"}" https://hooks.slack.com/services/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
#curl -X POST --data-urlencode "payload={\"channel\": \"#fault_notifications\", \"text\": \"test post.\", \"image_url\": \"https://www.dropbox.com/home/Engineering/Validation/Test%20Data/FaultPlotting/Plotted_Faults/M32-X-U27.6_MotorOverVoltage_2019-04-19T16-51-11.650000.png\"}" https://hooks.slack.com/services/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