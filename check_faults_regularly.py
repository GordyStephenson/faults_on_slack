# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 20:43:38 2019

@author: Test
"""

import time
import os
import checkForFaults as cff
import sys
import subprocess
import datetime as dt

dropboxDir = "https://www.dropbox.com/preview/Engineering/Validation/Test%20Data/FaultPlotting/Plotted_Faults/"
command1 = r'curl -X POST --data-urlencode "payload={\"channel\": \"fault_notifications\", \"text\": \"<'
#command2 = dropboxDir + fileStr + r'?role=work|' + textStr +'>' + r'\"}"'
command3 = ' https://hooks.slack.com/services/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'

checkFaultsFromTime = dt.datetime(year=2019,month=4,day=29,hour=23, minute=55)
# Store a list of the most recently plotted faults, so that program doesn't plot them
# more than once.
lastFewFaults = []

while True:
    # Dump the oldest half our list of recently plotted faults when it gets too long.
    if len(lastFewFaults)>60:
        lastFewFaults = lastFewFaults[30:]
    try:
        print('Checking for faults since ' + str(checkFaultsFromTime) )
        # Before checking, tag "now" -- in UTC -- as the new time to start checking.
        nextCheckStartTime = dt.datetime.now() + dt.timedelta(hours=7)
        print('Next start time is : '+str(nextCheckStartTime) )
        list_of_faults, list_of_faultFigs = cff.checkNHours(Nhours = 1, startTime = checkFaultsFromTime)
        checkFaultsFromTime = nextCheckStartTime
        print( 'Ok, try uploading and do it all again.') 
        for n in range(len(list_of_faults)):
            fileStr = list_of_faultFigs[n]
            fault = list_of_faults[n]
            if not fault in lastFewFaults:
                unitID, faultType, dateAndTime, location = fault.split('_')
                dateToks = dateAndTime.split('T')
                dateStr = dateToks[0] +' '+ dateToks[1].replace('-',':')[:-3]+' UTC'
                textStr = '  '.join([unitID, faultType, dateStr, location])
                #textStr = list_of_faults[n].replace('_','  ')
                colorOption = ''
                if 'Desat' in fault:
                    colorOption = r', \"color\":\"#ff0000\"'
                command2 = dropboxDir + fileStr + r'?role=work|' + textStr + r'>\"' + colorOption + r'}"'
                print( command1 + command2 + command3 )
                try:
                    subprocess.run( command1 + command2 + command3 )
                    lastFewFaults.append( fault )
                except Exception as e:
                    print(e)
                    print("couldn't upload "+ fileStr )
        time.sleep(60*12) ### Wait a little while before checking again.
    #except Exception as e:
    #    print( "There is a problem!!!!! Coming to you live from line 42 of the code!!!)
    #    print( e )                
    except KeyboardInterrupt:
        sys.exit(1)
